import {
  Box,
  Card,
  CardContent,
  CardMedia,
  Typography,
  Button,
  CardActions,
} from "@mui/material";
import Highlighter from "react-highlight-words";
import { Link } from "react-router-dom";

const EmploymentCard = (props) => {
  const { employment } = props;
  const highlightText = (input) => {
    if (props.wordToHighlight != null) {
      return (
        <Highlighter
          highlightClassName="HighlightText"
          searchWords={[props.wordToHighlight]}
          autoEscape={true}
          textToHighlight={input}
        />
      );
    }
    return input;
  };
  return (
    <Box
      sx={{
        justifySelf: "center",
        paddingTop: "20px",
        width: { xs: "40vw", md: "23.5vw" },
        height: "100%",
      }}
      key={employment.name}
    >
      <Card sx={{}}>
        <CardMedia
          sx={{ height: 140 }}
          image={employment.image}
          title={employment.imageTitle}
        />
        <CardContent>
          <Typography
            variant="h6"
            color="text.secondary"
            style={{
              textAlign: "center",
              fontsize: "100%",
              fontWeight: "bold",
            }}
          >
            {highlightText(employment.name)}
          </Typography>
          <Typography variant="body1" style={{ textAlign: "center" }}>
            <a href={employment.website} target="_blank">
              Website Link
            </a>
          </Typography>
          <Typography variant="body1">
            Location : {highlightText(employment.location)}
          </Typography>
        </CardContent>
        <CardActions>
          <Link to={"/employment-assistance/" + employment.id}>
            <Button size="small" target="_blank">
              Learn More
            </Button>
          </Link>
        </CardActions>
      </Card>
    </Box>
  );
};

export default EmploymentCard;
