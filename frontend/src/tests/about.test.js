import * as renderer from "react-test-renderer";
import About from "../components/About";
import { render, screen, cleanup } from "@testing-library/react";
import "@testing-library/jest-dom";
import { BrowserRouter } from "react-router-dom";
import Communities from "../components/Communities";
import Employment from "../components/Employment";
import LegalAid from "../components/LegalAid";

afterEach(() => {
  cleanup(); // Resets the DOM after each test suite
});

// Snapshot test
it("matches snapshot", () => {
  const aboutPage = renderer.create(<About />);
  let tree = aboutPage.toJSON();
  expect(tree).toMatchSnapshot();
});

// Unit Test 1
test("Testing About", () => {
  render(<About />, { wrapper: BrowserRouter });
  expect(screen.getByTestId("about-cards")).toBeInTheDocument();
});

// Unit Test 2
test("renders Meet Our Team!", () => {
  render(<About />);
  const aboutElement = screen.getByText(/Meet our Team!/i);
  expect(aboutElement).toBeInTheDocument();
});

// Unit Test 3
test("renders API Docs", () => {
  render(<About />);
  const aboutElement2 = screen.getByText(/API Docs/i);
  expect(aboutElement2).toBeInTheDocument();
});

// Unit Test 4
test("renders API docs href correctly", () => {
  render(<About />);
  const aboutElement2 = screen.getByText(/API Docs/i);
  expect(aboutElement2).toHaveAttribute(
    "href",
    "https://documenter.getpostman.com/view/30034938/2s9YJZ3jae",
  );
});

// Unit Test 5
test("renders Our Repo", () => {
  render(<About />);
  const aboutElement3 = screen.getByText(/Our Repo/i);
  expect(aboutElement3).toBeInTheDocument();
});

// Unit Test 6
test("renders Our Repo href correctly", () => {
  render(<About />);
  const aboutElement4 = screen.getByText(/Our Repo/i);
  expect(aboutElement4).toHaveAttribute(
    "href",
    "https://gitlab.com/varre.abhinav/undocumented-texas-immigrants",
  );
});

// Unit Test 7
test("renders Communities Page", () => {
  render(<Communities />);
});

// Unit Test 8
test("Communities Loading... placement", () => {
  render(<Communities />);
  const communitiesElement = screen.getByText(/Loading.../i);
  expect(communitiesElement).toBeInTheDocument();
});

// Unit Test 9
test("renders Employment Page", () => {
  render(<Employment />);
});

//Unit Test 10
test("Employment Loading... placement", () => {
  render(<Employment />);
  const employmentElement = screen.getByText(/Loading.../i);
  expect(employmentElement).toBeInTheDocument();
});

// Unit Test 11
test("renders Legal Aid Page", () => {
  render(<LegalAid />);
});

// Unit Test 12
test("Legal Aid Loading... placement", () => {
  render(<LegalAid />);
  const legalAidElement = screen.getByText(/Loading.../i);
  expect(legalAidElement).toBeInTheDocument();
});
