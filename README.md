# cs373 Group 16 names

Anthony Nguyen, Prerna Singh, Ben Wasden, Abhinav Varre, Yuanjing Yuan

## Name of project  

undocumented-texas-immigrants

## Website Link  
https://www.undocumentedtexasimmigrants.me/

## Most recent commit SHA  
57547298ff204cf7321adb4016d06eed48ef8ce6

## Estimated work time and actual work time  
Estimated: 50
Actual: 70

## Proposal

Our project aims at providing assistance to the undocumented immigrants near the Texas border. We aim to present information about where these immigrant communities are located, employment assistance and skill development for immigrants, and crucial legal resources as well. 

## Data Source URLs

https://www.migrationpolicy.org/
https://www.twc.texas.gov/jobseekers/employment-support-resources
https://www.causeiq.com/directory/immigrant-centers-list/texas-state/
https://guides.sll.texas.gov/immigration-law/legal-assistance
https://www.justia.com/lawyers/immigration-law/texas/legal-aid-and-pro-bono-services
https://www.immigrationadvocates.org/nonprofit/legaldirectory/search?state=TX
https://www.texastribune.org/2015/01/28/undocumented-population-demographics/
https://cmsny.org/resource-hub/
https://www.causeiq.com/directory/union-and-employment-assistance-providers-list/texas-state/
https://www2.ed.gov/about/overview/focus/immigration-resources.html
https://texasprojectfirst.org/en/employment-resources/

## The 3 Models

High-Density Areas of Undocumented Immigrants  
Legal Aid and Human Rights Advocacy Centers  
Employment Assistance and Skill Development Centers  

## Estimated number of instances

Densely-Populated Immigrant Communities:10  
Legal Aid Centers: 95  
Employment Assistance Centers: 30  

## Describe 5 attributes per model that can be filtered or sorted 

Immigrant communities
- Area Name (e.g., City, County, Neighborhood)
- Population Demographics (e.g., total population, percentage of immigrants)
- Cultural and Community Centers (e.g., community centers, places of worship)
- Local Businesses (e.g., immigrant-owned businesses, cultural restaurants)
- Housing Information (e.g., affordable housing options)

Legal aid
- Legal Organization Name
- Services Offered (e.g., immigration consultations, legal clinics)
- Contact Information (Phone, Email)
- Location (physical office address)
- Appointment Scheduling (if applicable)

Employment assistance and skills training
- Center Name
- Location (Physical Address)
- Contact Information (Phone, Email)
- Services Offered (e.g., job placement, resume building, interview skills, vocational training)
- Eligibility Criteria (if applicable)
- Operating Hours

## Connections between Instances of the Models

Instances in the “High-Density Areas of Undocumented Immigrants” model can link to instances in the “Legal Aid and Human Rights Advocacy Centers” and “Employment Assistance and Skill Development Centers” that are located in the same area. Instances in the “Legal Aid and Human Rights Advocacy Centers” can refer users to suitable “Employment Assistance and Skill Development Centers” instances and vice versa.

## Types of media for Instances of the Models

High-Density Areas of Undocumented Immigrants
- Map of areas densely populated by undocumented immigrants within the state
- News articles prevalent to the area/immigration in the area

Legal Aid and Human Rights Advocacy Centers
- Center logos and websites 
- Maps showing locations of centers 

Employment Assistance and Skill Development Centers
- Center logos and websites
- Feeds for job listings

## Questions our Site will answer
1. Where are the high-density areas of undocumented immigrants in the Texas border region, and what resources are available in these areas?
2. As an immigrant, where can I find the legal assistance I need? 
3. How can an immigrant find skill development tools/employment opportunities?

## Phase Leader 
Phase 1 : Anthony Nguyen 
Phase 2 : Abhinav Varre 
Phase 3 : Yuanjing Yuan & Prerna Singh 
Phase 4 : Ben Wasden  

Responsibilities: Leading meetings and making sure each task is being worked on/assigned 

