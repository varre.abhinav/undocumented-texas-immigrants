import unittest
from flask_api import app

class FlaskApiTests(unittest.TestCase):

	def setUp(self):
		self.app = app.test_client()

	def test_home(self):
		response = self.app.get('/')
		data = response.get_json()
		self.assertEqual(response.status_code, 200)
		self.assertEqual(data, "Welcome to the Undoc backend! Try a request like /communitiy to get started. If you'd like to get info about a specific instance, provide the key value pair ?id= at the end of your request.")

	def test_employment(self):
		response = self.app.get('/employment')
		data = response.get_json()
		self.assertEqual(response.status_code, 200)
		self.assertIsInstance(data, list)
	
	def test_community(self):
		response = self.app.get('/community')
		data = response.get_json()
		self.assertEqual(response.status_code, 200)
		self.assertIsInstance(data, list)

	def test_legal_aid(self):
		response = self.app.get('/legal_aid')
		data = response.get_json()
		self.assertEqual(response.status_code, 200)
		self.assertIsInstance(data, list)

	def test_employment_instance(self):
		response = self.app.get('/employment?id=1')
		data = response.get_json()
		self.assertEqual(response.status_code, 200)
		self.assertIsInstance(data, dict)
	
	def test_community_instance(self):
		response = self.app.get('/community?id=1')
		data = response.get_json()
		self.assertEqual(response.status_code, 200)
		self.assertIsInstance(data, dict)

	def test_legal_aid_instance(self):
		response = self.app.get('/legal_aid?id=1')
		data = response.get_json()
		self.assertEqual(response.status_code, 200)
		self.assertIsInstance(data, dict)

	def test_nearby_employment(self):
		response = self.app.get('/employment/nearby')
		data = response.get_json()
		self.assertEqual(response.status_code, 404)
	
	def test_nearby_community(self):
		response = self.app.get('/community/nearby')
		data = response.get_json()
		self.assertEqual(response.status_code, 404)

	def test_nearby_legal_aid(self):
		response = self.app.get('/legal_aid/nearby')
		data = response.get_json()
		self.assertEqual(response.status_code, 404)

	def test_employment_ids(self):
		response = self.app.get('/employment/ids')
		data = response.get_json()
		self.assertEqual(response.status_code, 200)
		self.assertIsInstance(data, dict)
	
	def test_community_ids(self):
		response = self.app.get('/community/ids')
		data = response.get_json()
		self.assertEqual(response.status_code, 200)
		self.assertIsInstance(data, dict)

	def test_legal_aid_ids(self):
		response = self.app.get('/legal_aid/ids')
		data = response.get_json()
		self.assertEqual(response.status_code, 200)
		self.assertIsInstance(data, dict)

	def test_search_community(self):
		response = self.app.get('/search/community?id=hou')
		data = response.get_json()
		self.assertEqual(response.status_code, 200)
		self.assertIsInstance(data, list)
	
	def test_search_legal_aid(self):
		response = self.app.get('/search/legalaid?id=hou')
		data = response.get_json()
		self.assertEqual(response.status_code, 200)
		self.assertIsInstance(data, list)

	def test_search_employment(self):
		response = self.app.get('/search/employment?id=hou')
		data = response.get_json()
		self.assertEqual(response.status_code, 200)
		self.assertIsInstance(data, list)
	
	def test_global_search(self):
		response = self.app.get('/search?id=hou')
		data = response.get_json()
		self.assertEqual(response.status_code, 200)
		self.assertIsInstance(data, dict)

if __name__ == '__main__':
	unittest.main()