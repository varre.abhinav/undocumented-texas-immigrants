import requests
from bs4 import BeautifulSoup
import json


def get_data():
    links = [
        "https://www.migrationpolicy.org/data/unauthorized-immigrant-population/county/48029",
        "https://www.migrationpolicy.org/data/unauthorized-immigrant-population/county/48061",
        "https://www.migrationpolicy.org/data/unauthorized-immigrant-population/county/48085",
        "https://www.migrationpolicy.org/data/unauthorized-immigrant-population/county/48113",
        "https://www.migrationpolicy.org/data/unauthorized-immigrant-population/county/48121",
        "https://www.migrationpolicy.org/data/unauthorized-immigrant-population/county/48141",
        "https://www.migrationpolicy.org/data/unauthorized-immigrant-population/county/48157",
        "https://www.migrationpolicy.org/data/unauthorized-immigrant-population/county/48201",
        "https://www.migrationpolicy.org/data/unauthorized-immigrant-population/county/48215",
        "https://www.migrationpolicy.org/data/unauthorized-immigrant-population/county/48245",
        "https://www.migrationpolicy.org/data/unauthorized-immigrant-population/county/48339",
        "https://www.migrationpolicy.org/data/unauthorized-immigrant-population/county/48439",
        "https://www.migrationpolicy.org/data/unauthorized-immigrant-population/county/48453",
        "https://www.migrationpolicy.org/data/unauthorized-immigrant-population/county/48479",
        "https://www.migrationpolicy.org/data/unauthorized-immigrant-population/county/48491",
    ]

    data = {}
    for link in links:
        response = requests.get(link)
        soup = BeautifulSoup(response.content, "html.parser")
        county_name_element = soup.select_one("span.field-content h1")
        if county_name_element:
            county_name = county_name_element.text.strip()

            data[county_name] = {}
            table = soup.find("table", {"id": "unauthorized-table"})
            if table:
                header = None
                subheader = None
                for row in table.find("tbody").find_all("tr"):
                    cells = row.find_all("td")
                    if "datasheet-heading" in row.get("class", []):
                        header = cells[0].text.strip()
                        data[county_name][header] = {}
                    elif "datasheet-subheading" in row.get("class", []):
                        subheader = cells[0].text.strip()
                        data[county_name][header][subheader] = []
                    elif len(cells) > 1:
                        val1 = cells[0].text.strip()
                        val2 = cells[1].text.strip()
                        val3 = cells[2].text.strip()
                        if val1 != "-":
                            if subheader is None:
                                data[county_name][header][val1] = [(val2, val3)]
                            else:
                                data[county_name][header][subheader].append(
                                    (val1, val2, val3)
                                )
    return data


def store_data(data):
    file_path = "./backend/data/communities.json"
    with open(file_path, "w") as file:
        json.dump(data, file)


if __name__ == "__main__":
    communities_data = get_data()
    store_data(communities_data)
