# app packages
beautifulsoup4==4.11.2
flask-restless-ng==2.5.0
flask-sqlalchemy==3.0.3
flask==2.2.3
flask-cors
requests==2.28.2
sqlalchemy >=2.0.20
Werkzeug==2.2.2
uszipcode
geopy
psycopg2-binary >=2.9.7
aws-wsgi
zappa



# dev packages
black==23.1.0
coverage==7.2.1
flake8==6.0.0
mypy==1.0.1
types-beautifulsoup4==4.11.6.7
