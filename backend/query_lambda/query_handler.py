import psycopg2
import json
from psycopg2.extras import RealDictCursor

host = "database-1.c52fpeqjbwao.us-east-1.rds.amazonaws.com"
username = "postgres"
password = "groupproject16"
database = "postgres"

conn = psycopg2.connect(
    host=host,
    database=database,
    user=username,
    password=password
)

def query_equal(cur, table, column, value):
    cur.execute(f"select * from {table} where lower({column}) like any (values ('%{value}%'))")
    result = cur.fetchall()
    return json.dumps(result)

def query_all(cur, table):
    cur.execute(f"select * from {table}")
    result = cur.fetchall()
    return json.dumps(result)

def query_keyword(cur, table, column, value):
    cur.execute(f"select * from {table} where lower({column})=lower('{value}')")
    result = cur.fetchall()
    return json.dumps(result)

def lambda_handler(event, context):
    cur = conn.cursor(cursor_factory = RealDictCursor)
    query_type = event['queryStringParameters']['query_type']
    query_table = event['queryStringParameters']['table']
    query_column = event['queryStringParameters']['column']
    query_value = event['queryStringParameters']['value']
    dump = None
    if query_type == 'all':
        dump = query_all(cur, query_table)
    elif query_type == 'equal':
        dump = query_equal(cur, query_table, query_column, query_value)
    elif query_type == 'keyword':
        dump = query_equal(cur, query_table, query_column, query_value)
    return {
        'statusCode':200,
        'body':dump
    }
