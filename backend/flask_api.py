from flask import Flask, jsonify, request
from flask_cors import CORS
from sqlalchemy import (
    create_engine,
    func,
    literal,
    or_,
    case,
    cast,
    Integer,
    literal_column,
)
from sqlalchemy.orm import sessionmaker
import re

# import awsgi
from database import database


app = Flask(__name__)
CORS(app)

DATABASE_URI = "postgresql://postgres:groupproject16@database-1.c52fpeqjbwao.us-east-1.rds.amazonaws.com:5432/postgres"
engine = create_engine(DATABASE_URI)
Session = sessionmaker(bind=engine)


# landing page route for the API
@app.route("/", methods=["GET"])
def home():
    return jsonify(
        "Welcome to the Undoc backend! Try a request like /communitiy to get started. If you'd like to get info about a specific instance, provide the key value pair ?id= at the end of your request."
    )


# searches through the community model for instances with a keyword that matches search id
def search_community(session, search_id):
    # threshold = 13
    keyword = search_id.lower()
    # communities = session.query(database.Communities).filter(
    #     or_(
    #         func.levenshtein(func.left(func.lower(database.Communities.county_name), 255), keyword) < threshold,
    #         func.levenshtein(func.left(func.lower(database.Communities.countries_of_birth_categories), 255), keyword) < threshold,
    #         func.levenshtein(func.left(func.lower(database.Communities.countries_of_birth_numbers), 255), keyword) < threshold,
    #         func.levenshtein(func.left(func.lower(database.Communities.years_of_us_residence_categories), 255), keyword) < threshold,
    #         func.levenshtein(func.left(func.lower(database.Communities.years_of_us_residence_numbers), 255), keyword) < threshold,
    #         func.levenshtein(func.left(func.lower(database.Communities.age_categories), 255), keyword) < threshold,
    #         func.levenshtein(func.left(func.lower(database.Communities.age_numbers), 255), keyword) < threshold
    #     )
    # ).order_by(
    #     func.levenshtein(func.left(func.lower(database.Communities.county_name), 255), keyword),
    #         func.levenshtein(func.left(func.lower(database.Communities.countries_of_birth_categories), 255), keyword),
    #         func.levenshtein(func.left(func.lower(database.Communities.countries_of_birth_numbers), 255), keyword),
    #         func.levenshtein(func.left(func.lower(database.Communities.years_of_us_residence_categories), 255), keyword),
    #         func.levenshtein(func.left(func.lower(database.Communities.years_of_us_residence_numbers), 255), keyword),
    #         func.levenshtein(func.left(func.lower(database.Communities.age_categories), 255), keyword),
    #         func.levenshtein(func.left(func.lower(database.Communities.age_numbers), 255), keyword)
    # ).all()
    communities = (
        session.query(database.Communities)
        .filter(
            or_(
                database.Communities.county_name.ilike(f"%{search_id}%"),
                database.Communities.countries_of_birth_categories.ilike(
                    f"%{search_id}%"
                ),
                database.Communities.countries_of_birth_numbers.ilike(f"%{search_id}%"),
                database.Communities.years_of_us_residence_categories.ilike(
                    f"%{search_id}%"
                ),
                database.Communities.years_of_us_residence_numbers.ilike(
                    f"%{search_id}%"
                ),
                database.Communities.age_categories.ilike(f"%{search_id}%"),
                database.Communities.age_numbers.ilike(f"%{search_id}%"),
            )
        )
        .order_by(
            func.levenshtein(
                func.left(func.lower(database.Communities.county_name), 255), keyword
            ),
            func.levenshtein(
                func.left(
                    func.lower(database.Communities.countries_of_birth_categories), 255
                ),
                keyword,
            ),
            func.levenshtein(
                func.left(
                    func.lower(database.Communities.countries_of_birth_numbers), 255
                ),
                keyword,
            ),
            func.levenshtein(
                func.left(
                    func.lower(database.Communities.years_of_us_residence_categories),
                    255,
                ),
                keyword,
            ),
            func.levenshtein(
                func.left(
                    func.lower(database.Communities.years_of_us_residence_numbers), 255
                ),
                keyword,
            ),
            func.levenshtein(
                func.left(func.lower(database.Communities.age_categories), 255), keyword
            ),
            func.levenshtein(
                func.left(func.lower(database.Communities.age_numbers), 255), keyword
            ),
        )
        .all()
    )
    return communities


# API route that searches for community instances relevant to the search id
@app.route("/search/community", methods=["GET"])
def community_search():
    search_id = request.args.get("id")
    session = Session()
    communities = search_community(session, search_id)
    return [community.as_dict() for community in communities]


# searches through the legal aid model for instances with a keyword that matches search id
def search_legal_aid(session, search_id):
    # threshold = 15
    keyword = search_id.lower()
    # legalAid = session.query(database.legalAid).filter(
    #     or_(
    #         func.levenshtein(func.left(func.lower(database.legalAid.org_name), 255), keyword) < threshold,
    #         func.levenshtein(func.left(func.lower(database.legalAid.area_of_assistance), 255), keyword) < threshold,
    #         func.levenshtein(func.left(func.lower(database.legalAid.types_of_assistance), 255), keyword) < threshold,
    #         func.levenshtein(func.left(func.lower(database.legalAid.location), 255), keyword) < threshold,
    #         func.levenshtein(func.left(func.lower(database.legalAid.contact), 255), keyword) < threshold,
    #         func.levenshtein(func.left(func.lower(database.legalAid.website), 255), keyword) < threshold
    #     )
    # ).order_by(
    #     func.levenshtein(func.left(func.lower(database.legalAid.org_name), 255), keyword),
    #         func.levenshtein(func.left(func.lower(database.legalAid.area_of_assistance), 255), keyword),
    #         func.levenshtein(func.left(func.lower(database.legalAid.types_of_assistance), 255), keyword),
    #         func.levenshtein(func.left(func.lower(database.legalAid.location), 255), keyword),
    #         func.levenshtein(func.left(func.lower(database.legalAid.contact), 255), keyword),
    #         func.levenshtein(func.left(func.lower(database.legalAid.website), 255), keyword)
    # ).all()
    legalAid = (
        session.query(database.legalAid)
        .filter(
            or_(
                database.legalAid.org_name.ilike(f"%{search_id}%"),
                database.legalAid.area_of_assistance.ilike(f"%{search_id}%"),
                database.legalAid.types_of_assistance.ilike(f"%{search_id}%"),
                database.legalAid.location.ilike(f"%{search_id}%"),
                database.legalAid.contact.ilike(f"%{search_id}%"),
                database.legalAid.website.ilike(f"%{search_id}%"),
            )
        )
        .order_by(
            func.levenshtein(
                func.left(func.lower(database.legalAid.org_name), 255), keyword
            ),
            func.levenshtein(
                func.left(func.lower(database.legalAid.area_of_assistance), 255),
                keyword,
            ),
            func.levenshtein(
                func.left(func.lower(database.legalAid.types_of_assistance), 255),
                keyword,
            ),
            func.levenshtein(
                func.left(func.lower(database.legalAid.location), 255), keyword
            ),
            func.levenshtein(
                func.left(func.lower(database.legalAid.contact), 255), keyword
            ),
            func.levenshtein(
                func.left(func.lower(database.legalAid.website), 255), keyword
            ),
        )
        .all()
    )
    return legalAid


# API route that searches for legal aid instances relevant to the search id
@app.route("/search/legalaid", methods=["GET"])
def legal_aid_search():
    search_id = request.args.get("id")
    session = Session()
    legalAids = search_legal_aid(session, search_id)
    return [legalAid.as_dict() for legalAid in legalAids]


# searches through the employment model for instances with a keyword that matches search id
def search_employment(session, search_id):
    # threshold = 5
    keyword = search_id.lower()
    # employemnts = session.query(database.employment).filter(
    #     or_(
    #         func.levenshtein(func.left(func.lower(database.employment.name), 255), keyword) < threshold,
    #         func.levenshtein(func.left(func.lower(database.employment.street), 255), keyword) < threshold,
    #         func.levenshtein(func.left(func.lower(database.employment.county), 255), keyword) < threshold,
    #         func.levenshtein(func.left(func.lower(database.employment.website), 255), keyword) < threshold,
    #         func.levenshtein(func.left(func.lower(database.employment.phone), 255), keyword) < threshold
    #     )
    # ).order_by(
    #     func.levenshtein(func.left(func.lower(database.employment.name), 255), keyword),
    #         func.levenshtein(func.left(func.lower(database.employment.street), 255), keyword),
    #         func.levenshtein(func.left(func.lower(database.employment.county), 255), keyword),
    #         func.levenshtein(func.left(func.lower(database.employment.website), 255), keyword),
    #         func.levenshtein(func.left(func.lower(database.employment.phone), 255), keyword)
    # ).all()
    employemnts = (
        session.query(database.employment)
        .filter(
            or_(
                database.employment.name.ilike(f"%{search_id}%"),
                database.employment.street.ilike(f"%{search_id}%"),
                database.employment.county.ilike(f"%{search_id}%"),
                database.employment.website.ilike(f"%{search_id}%"),
                database.employment.phone.ilike(f"%{search_id}%"),
            )
        )
        .order_by(
            func.levenshtein(
                func.left(func.lower(database.employment.name), 255), keyword
            ),
            func.levenshtein(
                func.left(func.lower(database.employment.street), 255), keyword
            ),
            func.levenshtein(
                func.left(func.lower(database.employment.county), 255), keyword
            ),
            func.levenshtein(
                func.left(func.lower(database.employment.website), 255), keyword
            ),
            func.levenshtein(
                func.left(func.lower(database.employment.phone), 255), keyword
            ),
        )
        .all()
    )
    return employemnts


# API route that searches for employment instances relevant to the search id
@app.route("/search/employment", methods=["GET"])
def employment_search():
    search_id = request.args.get("id")
    session = Session()
    employments = search_employment(session, search_id)
    return [employment.as_dict() for employment in employments]


# Searches for an instance that matches the given string
@app.route("/search", methods=["GET"])
def search():
    search_id = request.args.get("id")
    session = Session()
    if search_id:
        communities = search_community(session, search_id)
        employments = search_employment(session, search_id)
        legalAids = search_legal_aid(session, search_id)
    session.close()
    return {
        "Communities": [community.as_dict() for community in communities],
        "Employment": [employment.as_dict() for employment in employments],
        "Legal Aid": [legalAid.as_dict() for legalAid in legalAids],
    }


# Gets all info about the communities of undocumented immigrants, or a specific community if the key value pair id= is provided
@app.route("/community", methods=["GET"])
def get_community():
    id = request.args.get("id")
    session = Session()
    if id:
        community = session.query(database.Communities).get(id)
        session.close()
        if community:
            return jsonify(community.as_dict())
        return jsonify({"error": "Entry not found"}), 404
    sort_column = request.args.get("sort")
    order = request.args.get("order", "asc")

    query = session.query(database.Communities)
    if sort_column:
        special_sort_columns = [
            "countries_of_birth_numbers",
            "years_of_us_residence_numbers",
            "age_numbers",
        ]
        if sort_column in special_sort_columns:
            column_ref = getattr(database.Communities, sort_column)
            contains_comma = column_ref.op("LIKE")("%,%")

            sort_expression = case(
                (contains_comma, cast(func.split_part(column_ref, ",", 1), Integer)),
                else_=cast(column_ref, Integer),
            )
            if order == "desc":
                query = query.order_by(sort_expression.desc())
            else:
                query = query.order_by(sort_expression)
        else:
            column = getattr(database.Communities, sort_column)
            if column:
                if order == "desc":
                    query = query.order_by(column.desc())
                else:
                    query = query.order_by(column)

    community_instances = query.all()
    session.close()
    return jsonify([community.as_dict() for community in community_instances])


# Gets nearby related instances to the given community instance
@app.route("/community/nearby", methods=["GET"])
def get_nearby_for_community():
    id = request.args.get("id")
    if not id:
        return jsonify({"error": "Provide county id"}), 404
    session = Session()
    community = session.query(database.Communities).get(id)
    if not community:
        session.close()
        return jsonify({"error": "Community instance not found"}), 404

    nearby_employments = (
        session.query(database.employment).filter_by(community_id=id).all()
    )
    nearby_legalAid = session.query(database.legalAid).filter_by(community_id=id).all()
    session.close()
    return jsonify(
        {
            "nearby_employments": [
                employment.as_dict() for employment in nearby_employments
            ],
            "nearby_legalAids": [legalAid.as_dict() for legalAid in nearby_legalAid],
        }
    )


# Gets all the ids of the communities
@app.route("/community/ids", methods=["GET"])
def get_community_ids():
    session = Session()
    results = session.query(
        database.Communities.id, database.Communities.county_name
    ).all()
    session.close()

    data = {result[0]: result[1] for result in results}
    return jsonify(data)


# Gets all info about employment resources, or a specific employment instance if the key value pair id= is provided
@app.route("/employment", methods=["GET"])
def get_employment():
    id = request.args.get("id")
    session = Session()
    if id:
        employment = session.query(database.employment).get(id)
        session.close()
        if employment:
            return jsonify(employment.as_dict())
        return jsonify({"error": "Entry not found"}), 404

    sort_column = request.args.get("sort")
    order = request.args.get("order", "asc")
    filter_column = request.args.get("filter")
    filter_value = request.args.get("value")
    # sorts by the provided database table column
    if sort_column:
        query = session.query(database.employment)
        column = getattr(database.employment, sort_column, None)
        if column:
            if order == "desc":
                query = query.order_by(column.desc())
            else:
                query = query.order_by(column)
            employment_instances = query.all()
            session.close()
            return jsonify(
                [employment.as_dict() for employment in employment_instances]
            )
        else:
            return jsonify({"error": "invalid sort column"}), 400
    elif filter_column:
        # filters by zipcode
        if filter_column == "zipcode":
            employment_instances = (
                session.query(database.employment)
                .filter(database.employment.zip == filter_value)
                .all()
            )
            return jsonify(
                [employment.as_dict() for employment in employment_instances]
            )
        # filters by phone area code
        elif filter_column == "phone":
            phone_extension = database.employment.phone.contains(
                "(" + filter_value + ")"
            )
            employment_instances = (
                session.query(database.employment).filter(phone_extension).all()
            )
            return jsonify(
                [employment.as_dict() for employment in employment_instances]
            )
        # filters by whether instance has a website
        elif filter_column == "website":
            has_website = database.employment.website.contains("www.")
            employment_instances = (
                session.query(database.employment).filter(has_website).all()
            )
            return jsonify(
                [employment.as_dict() for employment in employment_instances]
            )
    else:
        employment_instances = session.query(database.employment).all()
        session.close()
        return jsonify([employment.as_dict() for employment in employment_instances])


# Gets nearby related instances to the given employment instance
@app.route("/employment/nearby", methods=["GET"])
def get_nearby_for_employment():
    id = request.args.get("id")
    if not id:
        return jsonify({"error": "Provide employment id"}), 404
    session = Session()
    employment = session.query(database.employment).get(id)
    if not employment:
        session.close()
        return jsonify({"error": "Employment instance not found"}), 404
    community_id = employment.community_id
    # gets the nearby instances with the same community id
    nearby_communities = (
        session.query(database.Communities).filter_by(id=community_id).all()
    )
    nearby_legalAid = (
        session.query(database.legalAid).filter_by(community_id=community_id).all()
    )
    session.close()
    return jsonify(
        {
            "nearby_communities": [
                community.as_dict() for community in nearby_communities
            ],
            "nearby_legalAids": [legalAid.as_dict() for legalAid in nearby_legalAid],
        }
    )


# Gets all the ids of the employment instances
@app.route("/employment/ids", methods=["GET"])
def get_employment_ids():
    session = Session()
    results = session.query(database.employment.id, database.employment.name).all()
    session.close()

    data = {result[0]: result[1] for result in results}
    return jsonify(data)


# Gets all info about legal aid resources, or a specific legal aid instance if the key value pair id= is provided
@app.route("/legal_aid", methods=["GET"])
def get_legal_aid():
    id = request.args.get("id")
    session = Session()
    if id:
        legal_aid = session.query(database.legalAid).get(id)
        session.close()
        if legal_aid:
            return jsonify(legal_aid.as_dict())
        return jsonify({"error": "Entry not found"}), 404

    sort_column = request.args.get("sort")
    order = request.args.get("order", "asc")
    filter_column = request.args.get("filter")
    filter_value = request.args.get("value")

    query = session.query(database.legalAid)
    if sort_column:
        # extracts zipcode from location field and sorts by those
        if sort_column == "zipcode":
            zip_code_expression = func.right(database.legalAid.location, 5)
            zip_code_expression = cast(zip_code_expression, Integer)

            if order == "desc":
                query = query.order_by(zip_code_expression.desc())
            else:
                query = query.order_by(zip_code_expression)
        # handles all other sorting
        else:
            column = getattr(database.legalAid, sort_column, None)
            if column:
                if order == "desc":
                    query = query.order_by(column.desc())
                else:
                    query = query.order_by(column)
    elif filter_column:
        # filters by zipcode
        if filter_column == "zipcode":
            zip_code_expression = func.right(database.legalAid.location, 5)
            legal_aid_instances = query.filter(
                zip_code_expression == filter_value
            ).all()
            return jsonify([legal_aid.as_dict() for legal_aid in legal_aid_instances])
        # filters by phone's area code
        elif filter_column == "phone":
            phone_extension = database.legalAid.contact.contains(filter_value)
            legal_aid_instances = query.filter(phone_extension).all()
            return jsonify([legal_aid.as_dict() for legal_aid in legal_aid_instances])
        # filters by whether or not instance has website
        elif filter_column == "website":
            has_website = database.legalAid.website.contains('http')
            legal_aid_instances = query.filter(has_website).all()
            return jsonify([legal_aid.as_dict() for legal_aid in legal_aid_instances])
    legal_aid_instances = query.all()
    session.close()
    return jsonify([legal_aid.as_dict() for legal_aid in legal_aid_instances])


# Gets nearby related instances to the given legal aid instance
@app.route("/legal_aid/nearby", methods=["GET"])
def get_nearby_for_legal_aid():
    id = request.args.get("id")
    if not id:
        return jsonify({"error": "Provide legal aid id"}), 404
    session = Session()
    legalAid = session.query(database.legalAid).get(id)
    if not legalAid:
        session.close()
        return jsonify({"error": "Legal aid instance not found"}), 404
    community_id = legalAid.community_id
    # gets the nearby instances that have the same community id
    nearby_communities = (
        session.query(database.Communities).filter_by(id=community_id).all()
    )
    nearby_employments = (
        session.query(database.employment).filter_by(community_id=id).all()
    )
    session.close()
    return jsonify(
        {
            "nearby_communities": [
                community.as_dict() for community in nearby_communities
            ],
            "nearby_employments": [
                employment.as_dict() for employment in nearby_employments
            ],
        }
    )


# Gets all the ids of the legal aid instances
@app.route("/legal_aid/ids", methods=["GET"])
def get_legal_aid_ids():
    session = Session()
    results = session.query(database.legalAid.id, database.legalAid.org_name).all()
    session.close()

    data = {result[0]: result[1] for result in results}
    return jsonify(data)


def lambda_handler(event, context):
    return awsgi.response(app, event, context, base64_content_types={"image/png"})


if __name__ == "__main__":
    app.run(debug=True)
