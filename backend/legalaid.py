import requests
from bs4 import BeautifulSoup
import json


def get_data():
    base_url = (
        "https://www.immigrationadvocates.org/nonprofit/legaldirectory/search?state=TX"
    )

    legal_centers = []
    url = base_url

    while url:
        response = requests.get(url)

        soup = BeautifulSoup(response.content, "html.parser")

        directory_results = soup.find(id="directory-search-results")

        if directory_results:
            directory_contents = directory_results.find_all(
                "div", class_="directory-organization-summary"
            )
            for div in directory_contents:
                org_name_tag = div.find("h4")
                org_name = (
                    org_name_tag.a.text.strip()
                    if org_name_tag and org_name_tag.a
                    else None
                )

                areas_of_assistance = (
                    div.find("td", string="Areas of legal assistance:")
                    .find_next_sibling("td")
                    .text.strip()
                    if div.find("td", string="Areas of legal assistance:")
                    else None
                )

                types_of_assistance = (
                    div.find("td", string="Types of legal assistance:")
                    .find_next_sibling("td")
                    .text.strip()
                    if div.find("td", string="Types of legal assistance:")
                    else None
                )

                location = (
                    div.find("td", string="Location:")
                    .find_next_sibling("td")
                    .text.strip()
                    if div.find("td", string="Location:")
                    else None
                )

                contact_tag = div.find("td", string="Contact:")
                contact = (
                    contact_tag.find_next_sibling("td").text.strip()
                    if contact_tag
                    else None
                )

                website = (
                    contact_tag.find_next_sibling("td").a.get("href")
                    if contact_tag and contact_tag.find_next_sibling("td").a
                    else None
                )

                organization = {
                    "name": org_name,
                    "areas_of_assistance": areas_of_assistance,
                    "types_of_assistance": types_of_assistance,
                    "location": location,
                    "contact": contact,
                    "website": website,
                }

                legal_centers.append(organization)
        next_link = soup.find("a", class_="next")
        url = f"{base_url}{next_link.get('href')}" if next_link else None

    return legal_centers


def store_data(data):
    file_path = "./backend/data/legal_centers.json"
    with open(file_path, "w") as file:
        json.dump(data, file)


if __name__ == "__main__":
    legal_centers = get_data()
    store_data(legal_centers)
