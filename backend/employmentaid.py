import requests
from bs4 import BeautifulSoup
import json

def get_data():
    base_url = (
        "https://www.googlyfish.com/employment-agencies/tx_others_employment_agencies/"
    )

    employment = []
    url = base_url

    while url:
        response = requests.get(url)

        search_url = "https://www.googlyfish.com"

        soup = BeautifulSoup(response.content, "html.parser")

        directory_results = soup.find("div", class_="col-md-9")
        directory_search = directory_results.find(class_="p-3 bg-white")
        if directory_search:
            directory_contents = directory_search.find_all("div", class_="listing-city")
            for div in directory_contents:
                link = div.find("a")
                cur_url =  f"{search_url}{link['href']}"
                cur_response = requests.get(cur_url)
                cur_soup = BeautifulSoup(cur_response.content, "html.parser")
                
                if cur_soup:
                    profile = cur_soup.find("div", class_="listing-entry")
                    name_tag = cur_soup.find("h1", class_="page-title")
                    name = (
                        name_tag.text.strip()
                        if name_tag
                        else None
                    )

                    addy_tag = profile.find("div", itemprop="address")
                    addy_tag_span = addy_tag.find_all("span")
                    address = ""
                    if addy_tag_span:
                        for text in addy_tag_span:
                            address += str(text.text.strip()) + " "

                    city_tag = addy_tag.find("span", itemprop="addressLocality")
                    city = (
                        city_tag.text.strip()
                        if city_tag
                        else None
                    )

                    phone_tag = profile.find("span", itemprop="telephone")
                    phone = (
                        phone_tag.text.strip()
                        if phone_tag
                        else None
                    )

                    link_tag = profile.find("span", itemprop="url")
                    link = (
                        link_tag.text.strip()
                        if link_tag
                        else None
                    )

                    information = {
                        "Name": name,
                        "Address": address,
                        "Phone Number": phone,
                        "Website": link,
                        "City": city
                    }

                    employment.append(information)
        next_links = soup.find("ul", class_="pagination pagination-sm justify-content-center text-center")
        next_link = next_links.find_all("li")[-1]
        a = next_link.find("a")
        url = f"{search_url}{a['href']}" if a else None


    return employment


def store_data(data):
    file_path = "./data/employment.json"
    with open(file_path, "w") as file:
        json.dump(data, file)


if __name__ == "__main__":
    employment = get_data()
    store_data(employment)

# def get_data():
#     base_url = (
#         "https://www.causeiq.com/directory/immigrant-centers-list/texas-state/"
#     )

#     employment = []
#     url = base_url
#     search_url = "https://www.causeiq.com"
#     response = requests.get(url)

#     soup = BeautifulSoup(response.content, "html.parser")

#     directory_results = soup.find(id="dir-main-content")
#     directory_search = directory_results.find(class_="directory-search")
#     if directory_search:

#         directory_contents = directory_search.find_all("h3")
#         for div in directory_contents:
#             link = div.find("a")
#             cur_url =  f"{search_url}{link['href']}"
#             cur_response = requests.get(cur_url)
#             cur_soup = BeautifulSoup(cur_response.content, "html.parser")
            
#             profile = cur_soup.find(id="org_profile")

#             if profile:
#                 org_name_tag = profile.find("h1", class_="m-t-0 text-red")
#                 org_name = (
#                     org_name_tag.text.strip()
#                     if org_name_tag
#                     else None
#                 )

#                 print(org_name)

#                 org_desc_tag = profile.find("div", class_="lead m-b-0")
#                 org_desc = (
#                     org_desc_tag.text.strip()
#                     if org_desc_tag
#                     else None
#                 )

#                 print(profile)
                
#                 information = {
#                     "name": org_name,
#                     "desc": org_desc,
#                 }


#                 org_info_out = profile.find("div", class_="col-sm-6")
#                 if org_info_out:
#                     org_info_in = org_info_out.find_all("dl")

#                     for index, data in enumerate(org_info_in):
#                         print("iterate")
#                         if data.name == 'dt':
#                             cur_item = data[index].text.strip()
#                             item_info = data[index + 1].text.strip()
#                             information[cur_item] = item_info

#                 employment.append(information)

#     return employment
