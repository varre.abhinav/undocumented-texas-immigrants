from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from sqlalchemy.orm import declarative_base, relationship
from sqlalchemy import Column, Integer, String, Float, ForeignKey

Base = declarative_base()


class Communities(Base):
    __tablename__ = "communities_table"
    id = Column(Integer, primary_key=True)
    county_name = Column(String, nullable=False, unique=True)
    unauthorized_population = Column(Integer, nullable=False)
    countries_of_birth_categories = Column(String)
    countries_of_birth_numbers = Column(String)
    years_of_us_residence_categories = Column(String)
    years_of_us_residence_numbers = Column(String)
    age_categories = Column(String)
    age_numbers = Column(String)
    photo_link = Column(String)
    latitude = Column(Float)
    longitude = Column(Float)
    legal_centers = relationship("legalAid", back_populates="community")
    employment_aid = relationship("employment", back_populates="community")

    def as_dict(self):
        return {c.name: getattr(self, c.name) for c in self.__table__.columns}


class legalAid(Base):
    __tablename__ = "legal_aid_table"
    id = Column(Integer, primary_key=True)
    community_id = Column(Integer, ForeignKey("communities_table.id"))
    org_name = Column(String, nullable=False, unique=True)
    area_of_assistance = Column(String)
    types_of_assistance = Column(String)
    location = Column(String)
    contact = Column(String)
    website = Column(String)
    photo_link = Column(String)
    latitude = Column(Float)
    longitude = Column(Float)
    community = relationship("Communities", back_populates="legal_centers")

    def as_dict(self):
        return {c.name: getattr(self, c.name) for c in self.__table__.columns}


class employment(Base):
    __tablename__ = "employment_table"
    id = Column(Integer, primary_key=True)
    community_id = Column(Integer, ForeignKey("communities_table.id"))
    name = Column(String, nullable=False, unique=True)
    street = Column(String)
    county = Column(String)
    zip = Column(Integer)
    website = Column(String, nullable=False)
    phone = Column(String)
    photo_link = Column(String)
    latitude = Column(Float)
    longitude = Column(Float)
    community = relationship("Communities", back_populates="employment_aid")

    def as_dict(self):
        return {c.name: getattr(self, c.name) for c in self.__table__.columns}
