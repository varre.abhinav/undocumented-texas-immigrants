import json
import requests
from database import Communities, legalAid, employment, Base
from sqlalchemy import create_engine, distinct
from sqlalchemy.orm import sessionmaker
from uszipcode import SearchEngine
import math
from geopy.geocoders import Photon
import re


# DATABASE_URI = "postgresql://postgres:groupproject16@database-1.c52fpeqjbwao.us-east-1.rds.amazonaws.com:5432/postgres"
DATABASE_URI = "postgresql://postgres:Ta1Tr1anh@localhost:5432/dev"
API_KEY = "14b943ccddbf4f9d88786bd618c54eb5"
search_url = "https://api.bing.microsoft.com/v7.0/images/search"
headers = {"Ocp-Apim-Subscription-Key" : API_KEY}

engine = create_engine(DATABASE_URI, echo=True)
Base.metadata.create_all(engine)
Session = sessionmaker(bind=engine)
session = Session()

search = SearchEngine()
county_centroids = None
counties = {}
closest_to_city = {}
closest_to_county = {}
lat_lon_cache = {}


def search_for_image(search_term):
    params  = {"q": search_term, "license": "public", "imageType": "photo"}
    response = requests.get(search_url, headers=headers, params=params)
    search_results = response.json()
    return search_results

def extract_city_from_address(address):
    # Define a regular expression pattern to match the city, assuming it follows the state abbreviation
    city_pattern = r',\s*([^,]+),\s*[A-Z]{2}\s+\d{5}'

    # Use re.search to find the city in the address
    match = re.search(city_pattern, address)

    # Check if a match was found
    if match:
        city = match.group(1)
        return city.strip()
    else:
        return None

def haversine_distance(lat1, lon1, lat2, lon2):
    # Radius of the Earth in kilometers
    earth_radius = 6371.0

    # Convert latitude and longitude from degrees to radians
    lat1_rad = math.radians(lat1)
    lon1_rad = math.radians(lon1)
    lat2_rad = math.radians(lat2)
    lon2_rad = math.radians(lon2)

    # Differences in latitude and longitude
    dlat = lat2_rad - lat1_rad
    dlon = lon2_rad - lon1_rad

    # Haversine formula
    a = math.sin(dlat/2)**2 + math.cos(lat1_rad) * math.cos(lat2_rad) * math.sin(dlon/2)**2
    c = 2 * math.atan2(math.sqrt(a), math.sqrt(1 - a))
    distance = earth_radius * c

    return distance

def find_closest_point(lat, lon):
    closest_point = None
    closest_distance = float('inf')

    for name, coords in counties.items():
        point_lat, point_lon = coords
        distance = haversine_distance(lat, lon, point_lat, point_lon)

        if distance < closest_distance:
            closest_distance = distance
            closest_point = name

    return closest_point

def find_item(list_of_dicts, key, item):
    # print("in find item")
    for dictionary in list_of_dicts:
        # print(key)
        # print(dictionary[key])
        # print(item)
        if key in dictionary and dictionary[key] == item:
            return dictionary
    print("didn't find")
    return None  # Return None if no matching dictionary is found

def populate_db():
    load_communities()
    load_legal_aid()
    populate_employment_aid()
    # print(counties)
    # populate_communities()
    # populate_legal_centers()
    # with open("backend/database/closest_to_county.json", "w") as file:
    #     json.dump(closest_to_county, file)
    # with open("backend/database/closest_to_city.json", "w") as file:
    #     json.dump(closest_to_city, file)
    # with open("backend/database/lat_long.json", "w") as file:
    #     json.dump(lat_lon_cache, file)
    # # print(closest_to_city)

def load_communities():
    with open("backend/database/community_with_coord.json") as jsn:
        communities = json.load(jsn)
        for community in communities:
            county = community['county_name']
            counties[county] = (community['longitude'], community['latitude'])
            communnity_instance = Communities(
                county_name=community['county_name'],
                unauthorized_population=community['unauthorized_population'],
                countries_of_birth_categories=community['countries_of_birth_categories'],
                countries_of_birth_numbers=community['countries_of_birth_numbers'],
                years_of_us_residence_categories=community['years_of_us_residence_categories'],
                years_of_us_residence_numbers=community['years_of_us_residence_numbers'],
                age_categories=community['age_categories'],
                age_numbers=community['age_numbers'],
                photo_link=community['photo_link'],
                latitude=community['latitude'],
                longitude=community['longitude']
            )
            if communnity_instance:
                session.add(communnity_instance)
                try:
                    session.commit()
                except:
                    session.rollback()

def load_legal_aid():
    with open("backend/database/legal_aid_with_coord.json") as jsn:
        legal_aid = json.load(jsn)
        for center in legal_aid:
            city = extract_city_from_address(center['location'])
            county = find_closest_point(center['longitude'], center['latitude'])
            closest_to_city[city] = county
            community = session.query(Communities).filter_by(county_name=county).first()
            legal_center_instance = legalAid(
                    org_name=center['org_name'],
                    area_of_assistance=center['area_of_assistance'],
                    types_of_assistance=center['types_of_assistance'],
                    location=center['location'],
                    contact=center['contact'],
                    website=center['website'],
                    community = community,
                    photo_link=center['photo_link'],
                    latitude=center['latitude'],
                    longitude=center['longitude']
                )
            if legal_center_instance:
                session.add(legal_center_instance)
                try:
                    session.commit()
                except:
                    session.rollback()

def load_employment():
    with open("employment_with_coord.json") as jsn:
        employment = json.load(jsn)
        for employ in employment:
            city = employ['county']
            county = find_closest_point(employ['longitude'], employ['latitude'])
            closest_to_county[city] = county
            community = session.query(Communities).filter_by(county_name=county).first()
            employment_instance = employment(
                name=employ['name'],
                street=employ['street'],
                county=employ['county'],
                zip=employ['zip'],
                website=employ['website'],
                phone=employ['phone'],
                community=community,
                photo_link=employ['photo_link'],
                latitude=employ['latitude'],
                longitude=employ['longitude']
            )
            if employment_instance:
                session.add(employment_instance)
                try:
                    session.commit()
                except:
                    session.rollback()




def populate_communities():
    with open("backend/data/communities.json") as jsn:
        communities_data = json.load(jsn)
        for community_name, community_data in communities_data.items():
            county_name = community_name.split(":")[1].split(",")[0].strip()
            with open("backend/database/community.json") as cur:
                calculated = json.load(cur)
            geolocator = Photon(user_agent="geoapiExercises", scheme="http")
            loc = geolocator.geocode(county_name)
            counties[county_name] = (loc.longitude, loc.latitude)
            
            data = find_item(calculated, 'county_name', county_name)

            if data == None:
                unauthorized_population = int(community_data['Demographics']['Unauthorized Population'][0][0].replace(',', '').replace('%', '').strip() or 0)
                countries_of_birth = [(item[0], int(item[1].replace(',', '').replace('%', '').strip() or 0)) for item in community_data['Demographics']['Top Countries of Birth'] if item[1] != '-']
                countries_of_birth_categories = ', '.join([str(item[0]) for item in countries_of_birth])
                countries_of_birth_numbers = ', '.join([str(item[1]) for item in countries_of_birth])
                years_of_us_residence = [(item[0], int(item[1].replace(',', '').replace('%', '').strip() or 0)) for item in community_data['Demographics']['Years of U.S. Residence'] if item[1] != '-']
                years_of_us_residence_categories = ', '.join([str(item[0]) for item in years_of_us_residence])
                years_of_us_residence_numbers = ', '.join([str(item[1]) for item in years_of_us_residence])
                age = [(item[0], int(item[1].replace(',', '').replace('%', '').strip() or 0)) for item in community_data['Demographics']['Age'] if item[1] != '-']
                age_categories = ', '.join([str(item[0]) for item in age])
                age_numbers = ', '.join([str(item[1]) for item in age])
                search_results = search_for_image(county_name)
                if search_results.get('value'):
                    image_url = search_results['value'][0]['contentUrl']
                else:
                    image_url = "No Image Found"
            else:
                unauthorized_population=data['unauthorized_population']
                countries_of_birth_categories=data['countries_of_birth_categories']
                countries_of_birth_numbers=data['countries_of_birth_numbers']
                years_of_us_residence_categories=data['years_of_us_residence_categories']
                years_of_us_residence_numbers=data['years_of_us_residence_numbers']
                age_categories=data['age_categories']
                age_numbers=data['age_numbers']
                image_url=data['photo_link']

            community_instance = Communities(
                county_name=county_name,
                unauthorized_population=unauthorized_population,
                countries_of_birth_categories=countries_of_birth_categories,
                countries_of_birth_numbers=countries_of_birth_numbers,
                years_of_us_residence_categories=years_of_us_residence_categories,
                years_of_us_residence_numbers=years_of_us_residence_numbers,
                age_categories=age_categories,
                age_numbers=age_numbers,
                photo_link=image_url,
                latitude=loc.latitude,
                longitude=loc.longitude
            )
            if community_instance:
                session.add(community_instance)
                try:
                    session.commit()
                except:
                    session.rollback()
                

def populate_legal_centers():
    with open("backend/data/legal_centers.json") as jsn:
        legal_centers_data = json.load(jsn)
        for legal_center in legal_centers_data:
            org_name = legal_center['name']

            with open("backend/database/legal_aid.json") as cur:
                calculated = json.load(cur)

            data = find_item(calculated, 'org_name', org_name)

            location = legal_center['location']
            city = extract_city_from_address(location)

            if data:
                geolocator = Photon(user_agent="geoapiExercises", scheme="http")
                loc = geolocator.geocode(city)
                county = find_closest_point(loc.longitude, loc.latitude)
                closest_to_city[city] = county
                community = session.query(Communities).filter_by(county_name=county).first()

                legal_center_instance = legalAid(
                    org_name=org_name,
                    area_of_assistance=data['area_of_assistance'],
                    types_of_assistance=data['types_of_assistance'],
                    location=data['location'],
                    contact=data['contact'],
                    website=data['website'],
                    community = community,
                    photo_link=data['photo_link'],
                    latitude=loc.latitude,
                    longitude=loc.longitude
                )
                session.add(legal_center_instance)
                try:
                    session.commit()
                except:
                    session.rollback()


            

            # areas_of_assistance = legal_center['areas_of_assistance']
            # types_of_assistance = legal_center['types_of_assistance']
            
			
            


            # community = session.query(Communities).filter_by(county_name=county).first()
            # contact = legal_center['contact']
            # website = legal_center['website']


            # print(data)
            # if data:
            #     image_url=data['photo_link']
            # else:
            #     search_results = search_for_image(org_name)
            #     if search_results.get('value'):
            #         image_url = search_results['value'][0]['contentUrl']
            #     else:
            #         image_url = "No Image Found"

            # if website in contact:
            #     contact = contact.replace(website, "").strip(", ")

            # if county:
                # legal_center_instance = legalAid(
                #     org_name=org_name,
                #     area_of_assistance=areas_of_assistance,
                #     types_of_assistance=types_of_assistance,
                #     location=location,
                #     contact=contact,
                #     website=website,
                #     community = community,
                #     photo_link=image_url,
                #     latitude=loc.latitude,
                #     longitude=loc.longitude
                # )
            #     if legal_center_instance:
                    # session.add(legal_center_instance)
                    # try:
                    #     session.commit()
                    #     print("commit")
                    # except:
                    #     session.rollback()

            

            

def populate_employment_aid():
    with open("backend/data/employment.json") as jsn:
        employment_data = json.load(jsn)
        for employ in employment_data:
            name = employ['Name']
            with open("backend/database/employment.json") as cur:
                calculated = json.load(cur)
            
            data = find_item(calculated, 'name', name)
            print(data)

            if data:
                address = data['street']
                geolocator = Photon(user_agent="geoapiExercises", scheme="http")
                loc = geolocator.geocode(address)
                if loc:
                    county = find_closest_point(loc.longitude, loc.latitude)
                    community = session.query(Communities).filter_by(county_name=county).first()
                    if data['photo_link'] == "No Image Found":
                        search_results = search_for_image(name)
                        if search_results.get('value'):
                            photo = search_results['value'][0]['contentUrl']
                        else:
                            photo = "No Image Found"
                    else:
                        photo = data['photo_link']
                    employment_instance = employment(
                        name=name,
                        street=data['street'],
                        county=data['county'],
                        zip=data['zip'],
                        website=data['website'],
                        phone=data['phone'],
                        community=community,
                        photo_link=photo,
                        latitude=loc.latitude,
                        longitude=loc.longitude
                    )
                    if employment_instance:
                        session.add(employment_instance)
                        try:
                            session.commit()
                        except:
                            session.rollback()
                



            # address = employ['Address']
            # zip = (address[-6:]
            #         if address[-6:]
            #         else None)
            # phone = employ['Phone Number']
            # website = employ['Website']
            # city = employ['City']
            # if city in closest_to_county:
            #     county = closest_to_county[city]
            #     loc = lat_lon_cache[city]
            # else:
            #     geolocator = Photon(user_agent="geoapiExercises", scheme="http")
            #     try:
            #         loc = geolocator.geocode(city)
            #         county = find_closest_point(loc.longitude, loc.latitude)
            #         if county:
            #             closest_to_county[city] = county
            #             lat_lon_cache[city] = loc
            #     except:
            #         county = None
            # community = session.query(Communities).filter_by(county_name=county).first()

            # search_results = search_for_image(name)


            # if data:
            #     print(data)
            #     image_url=data['photo_link']
            # else:
            #     if search_results.get('value'):
            #         image_url = search_results['value'][0]['contentUrl']
            #     else:
            #         image_url = "No Image Found"
            
            # if county:
            #     employment_instance = employment(
            #         name=name,
            #         street=address,
            #         county=city,
            #         zip=zip,
            #         website=website,
            #         phone=phone,
            #         community=community,
            #         photo_link=image_url,
            #         latitude=loc.latitude,
            #         longitude=loc.longitude
            #     )
            #     print("bout to add")
            #     if employment_instance:
            #         session.add(employment_instance)
            #         try:
            #             session.commit()
            #         except:
            #             session.rollback()
populate_db()


# session.close()